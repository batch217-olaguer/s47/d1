// console.log("Hello world")

const txtFirstName = document.querySelector("#txt-first-name"); /* querySelector can use id (use "#") and " . " for class */
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name"); // use arrow functions ( " => {}" ) to call them

const updateFullName = () => {

	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;

}

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName); // Listens to event. https://www.w3schools.com/jsref/dom_obj_event.asp - list of Events
// <input type="text" onkeyup="updateFullName()">
// <input type="text" onkepress="myFunction()">
